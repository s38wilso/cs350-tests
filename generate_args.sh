#!/bin/bash

set -eu
shopt -s lastpipe

# dump lines from test file into array
mapfile -t lines < args_tests.txt
numlines=$(wc -l < args_tests.txt)

i=0
temp=$(mktemp)
while [[ $i -lt $numlines ]]; do
    echo ${lines[$i]} | read test conf out argc argv
    printf "GENERATING OUTPUT FOR $test $argv $conf $out\n\n"
    ./run_args_test.sh "$test" "$conf" $argc $(echo "$argv") > "$out"
    (( i += 1 ))
    printf "\n"
done
rm "$temp"

printf "COMPLETE\n"
