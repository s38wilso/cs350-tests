#!/bin/bash

set -eu
shopt -s lastpipe

# dump lines from test file into array
mapfile -t lines < vm_stress_tests.txt
numlines=$(wc -l < vm_stress_tests.txt)

i=0
temp=$(mktemp)
while [[ $i -lt $numlines ]]; do
    echo ${lines[$i]} | read test conf out
    printf "GENERATING OUTPUT FOR $test $conf $out\n\n"
    ./run_vm_stress_test.sh "$test" "$conf" > "$out"
    (( i += 1 ))
    printf "\n"
done
rm "$temp"

printf "COMPLETE\n"
