#!/bin/bash

# Args: $1 = program to run
#       $2 = config file

sys161 -c "$2" kernel "$1;q" |
    sed 's/^[0-9]\{3\}k physical memory available/XXXk physical memory available/' |
    sed 's/[0-9]\.[0-9]\{9\}/X.XXXXXXXXX/' |
    sed 's/cpu[0-9]/cpuX/' |
    sed 's/ASST[0-3] #.*)/ASSTX #X)/' |
    sed -e '/^The system is halted.$/d'
