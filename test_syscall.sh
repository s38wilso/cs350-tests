#!/bin/bash

set -eu
shopt -s lastpipe

# dump lines from test file into array
mapfile -t lines < syscall_tests.txt
numlines=$(wc -l < syscall_tests.txt)

i=0
temp=$(mktemp)
while [[ $i -lt $numlines ]]; do
    echo ${lines[$i]} | read test conf out
    test -f "$out"
    printf "TESTING $test $conf $out\n\n"
    for j in {1..100}; do
        # only print OS/161 output on the first test
        if [[ $j -eq 1 ]]; then
            ./run_syscall_test.sh "$test" "$conf" > "$temp"
        else
            ./run_syscall_test.sh "$test" "$conf" > "$temp" 2> /dev/null
        fi
        # custom function to deal with variance in hogparty
        if ! ./hogparty_cmp.sh "$temp" "$out" "$test"; then
            echo FAIL "$test" "$conf" "$out"
            diff -y "$temp" "$out"
        fi
    done
    printf "\nRAN $test $conf $out $j TIMES\n\n"
    (( i += 1 ))
done
rm "$temp"

printf "PASS\n"
