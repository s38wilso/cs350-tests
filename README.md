# CS 350 Test Harness (Winter 2021)

DISCLAIMER: This is not endorsed by the CS 350 course staff. Use at your own risk.

## Installation

These test files should be placed in the `root` directory of OS/161 (the place where you call `sys161 kernel` from). From that directory, you should probably be able to do something like
```bash
git clone https://git.uwaterloo.ca/s38wilso/cs350-tests.git
cp cs350-tests/* .
```

If you want to get really fancy, you could add a git submodule.

## Coverage

All tests for A2.

## Using

The `synch_tests.txt` and `syscall_tests.txt` store the tests to be run, one per line. Each line contains
- the name of the executable file to run
- the name of the configuration file to use
- the name of the file for output redirection
- (for argument passing) the number of arguments
- (for argument passing) the arguments.

1. Edit these files to include the tests that you want to run.
2. Generate output by running `./generate_synch.sh` and `./generate_syscall.sh`. This will run each test once and populate the appropriate output file.
3. **Manually verify that the output is correct.**
4. Run `test_synch.sh` and `test_syscall.sh` to run each test 100 times and compare with the output generated in step 2.

The `hogparty_cmp.sh` script contains a custom comparison script to account for the non-determinism of the `hogparty` test. The `run_synch_test.sh` and `run_syscall_test.sh` scripts use `sed` to strip OS/161 output of specific numbers (such as running times) that differ between test runs, as well as lines that may execute in a different order. This allows for direct comparison in most cases.

## Contributing

Pull requests to improve the test harness are always appreciated.
