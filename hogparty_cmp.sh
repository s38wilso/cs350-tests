#!/bin/bash

set -eu

# $1: tempfile
# $2: outfile
# $3: test name
if [ "$3" == "uw-testbin/hogparty" ]; then

    #first check if number of lines agrees
    wc1=$(wc -l <$1)
    wc2=$(wc -l <$2)
    test $wc1 -eq $wc2

    # now check if head and tail agree
    # get number of leading and trailing lines
    head=$(sed -n '/^OS\/161 kernel: p uw-testbin\/hogparty$/=' "$2")
    tail=$(($wc1-$(sed -n '/^Operation took X.XXXXXXXXX seconds$/=' "$2")+1))

    # temporary files
    temp1=$(mktemp)
    temp2=$(mktemp)

    # put heads in temporary files
    cat "$1" | head -n$head > "$temp1"
    cat "$2" | head -n$head > "$temp2"
    cmp -s "$temp1" "$temp2"

    # put tails in temporary files
    cat "$1" | tail -n$tail > "$temp1"
    cat "$2" | tail -n$tail > "$temp2"
    cmp -s "$temp1" "$temp2"

    # get number of lines in middle
    mid=$(($wc1-$head-$tail))

    # now confirm that the number of characters in the middle of the tempfile is 21
    cat $1 | head -n$(($head+$mid)) | tail -n$mid > "$temp1"
    wc1=$(wc -c < "$temp1")
    test $wc1 -eq 21

    # should have 5 x's, 5 y's, 5 z's, 3 newlines, and 3 carriage returns
    x=$(tr -cd 'x' < "$temp1" | wc -c)
    y=$(tr -cd 'y' < "$temp1" | wc -c)
    z=$(tr -cd 'z' < "$temp1" | wc -c)
    r=$(tr -cd '' < "$temp1" | wc -c)
    n=$(tr -cd '\n' < "$temp1" | wc -c)
    test $x -eq 5
    test $y -eq 5
    test $z -eq 5
    test $r -eq 3
    test $n -eq 3

elif [ "$3" == "uw-testbin/widefork" ]; then

    #first check if number of lines agrees
    wc1=$(wc -l <$1)
    wc2=$(wc -l <$2)
    test $wc1 -eq $wc2

    # now check if head and tail agree
    # get number of leading and trailing lines
    head=$(sed -n '/^OS\/161 kernel: p uw-testbin\/widefork$/=' "$2")
    tail=$(($wc1-$(sed -n '/^Operation took X.XXXXXXXXX seconds$/=' "$2")+1))

    # temporary files
    temp1=$(mktemp)
    temp2=$(mktemp)

    # put heads in temporary files
    cat "$1" | head -n$head > "$temp1"
    cat "$2" | head -n$head > "$temp2"
    cmp -s "$temp1" "$temp2"

    # put tails in temporary files
    cat "$1" | tail -n$tail > "$temp1"
    cat "$2" | tail -n$tail > "$temp2"
    cmp -s "$temp1" "$temp2"

    # get number of lines in middle
    mid=$(($wc1-$head-$tail))

    # now confirm that the number of characters in the middle of the tempfile is 27
    cat $1 | head -n$(($head+$mid)) | tail -n$mid > "$temp1"
    wc1=$(wc -c < "$temp1")
    test $wc1 -eq 27

    # should have 3 P's, 1 A, 1 B, 1 C, 1 a, 1 b, 1 c, 9 newlines, and 9 carriage returns
    P=$(tr -cd 'P' < "$temp1" | wc -c)
    A=$(tr -cd 'A' < "$temp1" | wc -c)
    B=$(tr -cd 'B' < "$temp1" | wc -c)
    C=$(tr -cd 'C' < "$temp1" | wc -c)
    a=$(tr -cd 'a' < "$temp1" | wc -c)
    b=$(tr -cd 'b' < "$temp1" | wc -c)
    c=$(tr -cd 'c' < "$temp1" | wc -c)
    r=$(tr -cd '' < "$temp1" | wc -c)
    n=$(tr -cd '\n' < "$temp1" | wc -c)
    test $P -eq 3
    test $A -eq 1
    test $B -eq 1
    test $C -eq 1
    test $a -eq 1
    test $b -eq 1
    test $c -eq 1
    test $r -eq 9
    test $n -eq 9
elif [ "$3" == "uw-testbin/onefork" ]; then

    #first check if number of lines agrees
    wc1=$(wc -l <$1)
    wc2=$(wc -l <$2)
    test $wc1 -eq $wc2

    # now check if head and tail agree
    # get number of leading and trailing lines
    head=$(sed -n '/^OS\/161 kernel: p uw-testbin\/onefork$/=' "$2")
    tail=$(($wc1-$(sed -n '/^Operation took X.XXXXXXXXX seconds$/=' "$2")+1))

    # temporary files
    temp1=$(mktemp)
    temp2=$(mktemp)

    # put heads in temporary files
    cat "$1" | head -n$head > "$temp1"
    cat "$2" | head -n$head > "$temp2"
    cmp -s "$temp1" "$temp2"

    # put tails in temporary files
    cat "$1" | tail -n$tail > "$temp1"
    cat "$2" | tail -n$tail > "$temp2"
    cmp -s "$temp1" "$temp2"

    # get number of lines in middle
    mid=$(($wc1-$head-$tail))

    # now confirm that the number of characters in the middle of the tempfile is 21
    cat $1 | head -n$(($head+$mid)) | tail -n$mid > "$temp1"
    wc1=$(wc -c < "$temp1")
    test $wc1 -eq 6

    # should have a P, a C, two newlines, and two carriage returns
    P=$(tr -cd 'P' < "$temp1" | wc -c)
    C=$(tr -cd 'C' < "$temp1" | wc -c)
    r=$(tr -cd '' < "$temp1" | wc -c)
    n=$(tr -cd '\n' < "$temp1" | wc -c)
    test $P -eq 1
    test $C -eq 1
    test $r -eq 2
    test $n -eq 2
else
    cmp -s "$1" "$2"
fi
